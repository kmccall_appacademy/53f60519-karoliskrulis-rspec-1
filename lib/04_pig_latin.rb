
def translate(str)
  pigified = []
  str.split(' ').each do |word|
    pigified << to_pig(word)
  end
  pigified.join(' ')
end

def to_pig(word)
  consonants = /\b([bcdfghjklmnprstvwxyz]|qu)+/

  first_cons = word.match(consonants).to_s
  "#{word.sub(first_cons, '')}#{first_cons}ay"
end
