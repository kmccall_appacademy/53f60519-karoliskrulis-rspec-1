def add(a, b)
  a + b
end

def subtract(a, b)
  a - b
end

def sum(arr)
  return 0 if arr.empty?
  arr.reduce(:+)
end

def multiply(numbers)
  numbers.reduce(:*)
end

def power(num, exp)
  num ** exp
end

def factorial(num)
  return 1 if num == 0
  (1..num).reduce(:*)
end
